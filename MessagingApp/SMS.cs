﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace MessagingApp
{
    public enum Status { CREATED, SENT, RECEIVED };

    // SMS Class
    class SMS
    {        
        // Status can be accessed from outside, but changed only from within the class/subclass
        public Status status { get; protected set; }
        private string sender;
        public string Sender
        {
            get { return sender; }
            private set {
                string pattern = @"^0[0-9]{9,20}$";
                Regex rx = new Regex(pattern);
                if (rx.IsMatch(value))
                {
                    sender = value;
                }
                else
                {
                    throw new InvalidNumber(String.Format("Sender number entered '{0}' must start with 0 and contain 10 or more digits.", value));
                }                
            }
        }

        private string recipient;
        public string Recipient
        {
            get { return recipient; }
            private set
            {
                string pattern = @"^0[0-9]{9,20}$";
                Regex rx = new Regex(pattern);
                if (rx.IsMatch(value))
                {
                    recipient = value;
                }
                else
                {
                    throw new InvalidNumber(String.Format("Recipient number entered '{0}' must start with 0 and contain 10 or more digits.", value));
                }
            }
        }

        // Private field to hold the message data
        protected string Message;

        public readonly bool IsGroupoMessage;

        /// <summary>
        /// Create a new instance of the SMS class
        /// </summary>
        /// <param name="myNumber">The phone number of the sender</param>
        /// <param name="recipient">The phone number of the recipient</param>
        /// <param name="message">The body of the message</param>
        /// <param name="groupMessage">Is part of a group message?</param>
        public SMS(string myNumber, string recipient, string message, bool groupMessage = false)
        {
            Sender = myNumber;
            Recipient = recipient;
            Message = message;
            IsGroupoMessage = groupMessage;
            if(groupMessage)
            {
                Console.WriteLine("A new group message has been sent!\n\tFrom: '{0}'\n\tTo: '{1}'\n\tMessage: '{2}'\n\tIsGroupMessage: '{3}'", myNumber, recipient, message, groupMessage);
            }
            else
            {
                Console.WriteLine("A new message has been sent!\n\tFrom: '{0}'\n\tTo: '{1}'\n\tMessage: '{2}'\n\tIsGroupMessage: '{3}'", myNumber, recipient, message, groupMessage);
            }

            this.status = Status.CREATED;            
        }

        // set the status of a message to SENT
        public void SendMessage()
        {
            this.status = Status.SENT;
        }

        // display the content of a message
        public virtual void ReadMessage()
        {
            if(this.status == Status.SENT || this.status == Status.RECEIVED)
            {
                Console.WriteLine("\tFrom: {0}\n\tTo: {1} \n\tContents: {2}", Sender, Recipient, Message);
                this.status = Status.RECEIVED;
            }
            else
            {
                Console.WriteLine("\tMessage has not yet been sent.");
            }
            
        }
    }
}
