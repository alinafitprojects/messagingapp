﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingApp
{
    class InvalidNumber : Exception
    {
        public InvalidNumber(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}
