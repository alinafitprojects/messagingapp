﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagingApp
{
    public enum MediaType {NOATTACHMENT, AUDIO, VIDEO, PICTURE };

    class MMS : SMS
    {        
        private MediaType mediaType;
        private readonly string filename;       

        /// <summary>
        /// Create a new instance of the MMS class
        /// </summary>
        /// <param name="myNumber">The phone number of the sender</param>
        /// <param name="recipient">The phone number of the recipient</param>
        /// <param name="message">The body of the message</param>
        /// <param name="mediaType">The MediaType of the MMS. Can be one of: audio, video, picture or no attachment</param>
        /// <param name="filename">Attachment filename, if provided, otherwise is null</param>
        /// <param name="groupMessage">Is part of a group message?</param>
        public MMS(string myNumber, string recipient, string message, MediaType mediaType, string filename = null, bool groupMessage = false) 
            : base(myNumber, recipient, message, groupMessage)
        {
            this.mediaType = mediaType;
            this.filename = filename;

            showAttachment();

            this.status = Status.CREATED;

        }

        // display the MMS message content
        public override void ReadMessage()
        {
            if (this.status == Status.SENT || this.status == Status.RECEIVED)
            {
                Console.WriteLine("\tFrom: {0}\n\tTo: {1} \n\tContents: {2}", Sender, Recipient, Message);
                //Show the attachment media type and file name 
                Console.WriteLine("\tAttachement infomation: ");
                showAttachment();
                status = Status.RECEIVED;
            }
            else
            {
                Console.WriteLine("\tMessage has not yet been sent.");
            }

        }

        // display the attachment content
        public void showAttachment()
        {
            switch(this.mediaType)
            {
                case MediaType.AUDIO:
                    Console.WriteLine("\tAudio file '{0}' attached to message.", filename);
                    break;

                case MediaType.PICTURE:
                    Console.WriteLine("\tPicture file '{0}' attached to message.", filename);
                    break;

                case MediaType.VIDEO:
                    Console.WriteLine("\tVideo file '{0}' attached to message.", filename);
                    break;

                default:
                    Console.WriteLine("\tNo media attached.");
                    break;
            }
        }
    }
}
