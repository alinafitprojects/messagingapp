﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MessagingApp
{
    public enum HelpMenu { MAINMENU, SUBMENU }

    class MessagingApp
    {
        private static List<SMS> messages = new List<SMS>();
        private static ConsoleColor headerColor;
        private static ConsoleColor optionsColor;
        private static ConsoleColor statusColor;
        private static ConsoleColor messageColor;
        private static ConsoleColor errorColor;
        private static HelpMenu helpMenu;

        static void Main(string[] args)
        {
            // set application text colours
            Console.BackgroundColor = ConsoleColor.White;
            headerColor = ConsoleColor.DarkCyan;
            optionsColor = ConsoleColor.DarkYellow;
            statusColor = ConsoleColor.Green;
            messageColor = ConsoleColor.Blue;
            errorColor = ConsoleColor.Red;

            try
            {
                // add SMS/MMS objects to the messages list
                PopulateMessages();
            }
            catch (InvalidNumber e)
            {
                Console.WriteLine("Error occured!\n" + e.Message + "\nMessage could not be created");
            }

            bool showMainMenu = true;

            while (showMainMenu)
            {
                DisplayMainMenu();
                ConsoleKeyInfo keypressed = Console.ReadKey();

                string choice = keypressed.KeyChar.ToString().ToLower();

                switch (choice)
                {
                    // Show messages
                    case "1":
                        // display the list of existing messages
                        ShowAllMessages();

                        bool showMessageOptions = true;

                        while (showMessageOptions)
                        {
                            if (messages.Count > 0)
                            {
                                DisplaySubMenu();

                                ConsoleKeyInfo userchoice = Console.ReadKey();
                                string subChoice = userchoice.KeyChar.ToString().ToLower();

                                switch (subChoice)
                                {
                                    // read message
                                    case "1":
                                        Console.ForegroundColor = headerColor;
                                        Console.WriteLine("\nEnter message number and press enter: ");
                                        Console.ForegroundColor = optionsColor;
                                        try
                                        {
                                            int messageNumber = int.Parse(Console.ReadLine());
                                            Console.ForegroundColor = headerColor;
                                            Console.WriteLine("\n\tMessage {0} content: ", messageNumber);
                                            Console.ForegroundColor = messageColor;
                                            messages[messageNumber - 1].ReadMessage();
                                        }
                                        catch (ArgumentOutOfRangeException ex1)
                                        {
                                            // display exception message if message does not exist
                                            Console.ForegroundColor = ConsoleColor.Red;
                                            Console.WriteLine(ex1.Message);
                                            Console.WriteLine("Message number does not exist.");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                        }
                                        catch (Exception ex2)
                                        {
                                            // display error message for any other type of exceptions
                                            Console.ForegroundColor = ConsoleColor.Red;
                                            Console.WriteLine(ex2.Message);
                                            Console.WriteLine("Invalid number");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                        }
                                        break;

                                    // send message
                                    case "2":
                                        Console.ForegroundColor = headerColor;
                                        Console.WriteLine("\nEnter message number and press enter: ");
                                        try
                                        {
                                            int messageNumber = int.Parse(Console.ReadLine());
                                            messages[messageNumber - 1].SendMessage();
                                            Console.ForegroundColor = optionsColor;
                                            Console.WriteLine("Message {0} has now been sent.", messageNumber);
                                        }
                                        catch (ArgumentOutOfRangeException ex1)
                                        {
                                            // display exception message if message does not exist
                                            Console.ForegroundColor = errorColor;
                                            Console.WriteLine(ex1.Message);
                                            Console.WriteLine("Message number does not exist.");
                                        }

                                        catch (Exception ex2)
                                        {
                                            // display error message for any other type of exceptions
                                            Console.ForegroundColor = errorColor;
                                            Console.WriteLine(ex2.Message);
                                            Console.WriteLine("Invalid number.");
                                        }
                                        break;

                                    // delete message
                                    case "3":
                                        Console.ForegroundColor = headerColor;
                                        Console.WriteLine("\nEnter message number and press enter: ");
                                        Console.ForegroundColor = optionsColor;
                                        try
                                        {
                                            int messageNumber = int.Parse(Console.ReadLine());
                                            messages.RemoveAt(messageNumber - 1);
                                            Console.ForegroundColor = ConsoleColor.Magenta;
                                            Console.WriteLine("Message {0} has now been deleted.", messageNumber);

                                        }
                                        catch (ArgumentOutOfRangeException ex1)
                                        {
                                            // display exception message if message does not exist
                                            Console.ForegroundColor = errorColor;
                                            Console.WriteLine(ex1.Message);
                                            Console.WriteLine("Message number does not exist.");
                                        }

                                        catch (Exception ex2)
                                        {
                                            // display error message for any other type of exceptions
                                            Console.ForegroundColor = errorColor;
                                            Console.WriteLine(ex2.Message);
                                            Console.WriteLine("Invalid number.");
                                        }
                                        break;

                                    // back to Main Menu
                                    case "8":
                                        showMessageOptions = false;
                                        break;

                                    // show help
                                    case "9":
                                        helpMenu = HelpMenu.SUBMENU;
                                        DisplayHelp();
                                        showMessageOptions = true;
                                        break;

                                    // exit application
                                    case "0":
                                        ExitApp();
                                        break;

                                    // no valid option selected
                                    default:
                                        Console.ForegroundColor = errorColor;
                                        Console.WriteLine("\nInvalid selection. Enter a valid option.");
                                        break;
                                }                               
                            }
                        }
                        break;

                    // create message
                    case "2":
                        try
                        {
                            // add new message object to 'messages' list
                            messages.Add(CreateNewMessage());
                        }
                        catch (Exception e)
                        {
                            // display exception message
                            Console.WriteLine(e.Message);
                        }
                        break;

                    // show help
                    case "9":
                        helpMenu = HelpMenu.MAINMENU;
                        DisplayHelp();
                        showMainMenu = true;
                        break;

                    // exit application
                    case "0":
                        ExitApp();
                        showMainMenu = false;
                        break;

                    // no valid option selected
                    default:
                        Console.ForegroundColor = errorColor;
                        Console.WriteLine("\nInvalid option selected. Choose a valid option.");
                        break;
                }
            }
        }

        // Exit the Application
        private static void ExitApp()
        {
            Console.ForegroundColor = headerColor;
            Console.WriteLine("\nExiting application");
            Thread.Sleep(500);
            Environment.Exit(0);
        }

        // populate the messages list with SMS/MMS objects
        private static void PopulateMessages()
        {
            SMS sms1 = new SMS("0852963852", "0852741852", "This is my first SMS message! It is also part of a Group message", true);
            SMS sms2 = new SMS("0852741852", "0852963852", "This is a simple SMS message");
            SMS sms3 = new SMS("0852963852", "0852741852", "This is my second Group SMS message!", true);

            MMS mms1 = new MMS("0852789456", "0852987654", "This is an Audio MMS message", MediaType.AUDIO, "birds.wav", true);
            MMS mms2 = new MMS("0852987654", "0852789456", "This MMS message has No attachmemt", MediaType.NOATTACHMENT, null, true);

            messages.AddRange(new List<SMS>() { sms1, sms2, sms3, mms1, mms2 });
            Console.Clear();
        }

        // Display MainMenu options
        private static void DisplayMainMenu()
        {
            Console.ForegroundColor = headerColor;
            Console.WriteLine("\nSMS/MMS Application");
            Console.WriteLine("Select what to do:");
            Console.ForegroundColor = optionsColor;
            Console.WriteLine("\t1: Show all messages.");
            Console.WriteLine("\t2: Create new messages.");
            Console.WriteLine("\t9: Help.");
            Console.WriteLine("\t0: Exit.");
            Console.ForegroundColor = headerColor;
            Console.WriteLine("Select Option: ");
            Console.ForegroundColor = optionsColor;
        }

        // Display Messages Manipulation Sub Menu
        private static void DisplaySubMenu()
        {
            Console.ForegroundColor = headerColor;
            Console.WriteLine("Select what to do:");
            Console.ForegroundColor = optionsColor;
            Console.WriteLine("\t1: Read Message.");
            Console.WriteLine("\t2: Send Message.");
            Console.WriteLine("\t3: Delete Message.");
            Console.WriteLine("\t8: Back to Main Menu.");
            Console.WriteLine("\t9: Help.");
            Console.WriteLine("\t0: Exit.");
            Console.ForegroundColor = headerColor;
            Console.WriteLine("Select Option: ");
            Console.ForegroundColor = optionsColor;
        }

        // Display Help Messages based on menu navigation position
        private static void DisplayHelp()
        {
            if (helpMenu == HelpMenu.MAINMENU)
            {
                Console.ForegroundColor = headerColor;
                Console.WriteLine("\n******************");
                Console.WriteLine("* Help Main Menu *");
                Console.WriteLine("******************");
                Console.ForegroundColor = optionsColor;
                Console.WriteLine("\tPress '1' in order to see the existing messages and manipulate them.");
                Console.WriteLine("\tPress '2' in order to create a new message.");
                Console.WriteLine("\tPress '9' to view the Help Menu");
                Console.WriteLine("\tPress '0' to exit the SMS/MMS application");
            }
            else if (helpMenu == HelpMenu.SUBMENU)
            {
                Console.ForegroundColor = headerColor;
                Console.WriteLine("\n******************");
                Console.WriteLine("* Help Sub Menu *");
                Console.WriteLine("******************");
                Console.ForegroundColor = optionsColor;
                Console.WriteLine("\tPress '1', then enter message number in order to read it's content. Only messages with status 'SENT', 'RECEIVED' can be viewed.");
                Console.WriteLine("\tPress '2', then enter message number in order to change the message status from 'CREATED' to 'SENT'");
                Console.WriteLine("\tPress '3', then enter message number in order to delete the message from the system. !!!This action is ireversible!!!");
                Console.WriteLine("\tPress '8' to return to Main Menu");
                Console.WriteLine("\tPress '9' to view the Help Menu");
                Console.WriteLine("\tPress '0' to exit the SMS/MMS application");
            }
        }

        // Create a new Message SMS/MMS Object
        private static SMS CreateNewMessage()
        {
            MediaType attachmentType = MediaType.NOATTACHMENT;
            Console.ForegroundColor = headerColor;
            Console.WriteLine("\n************************");
            Console.WriteLine("* Create a new message *");
            Console.WriteLine("************************");
            Console.ForegroundColor = optionsColor;
            Console.WriteLine("Write the sender number and press enter: ");
            Console.ForegroundColor = messageColor;
            string sender = Console.ReadLine().Trim();

            Console.ForegroundColor = optionsColor;
            Console.WriteLine("Write the receiver number and press enter: ");
            Console.ForegroundColor = messageColor;
            string recipient = Console.ReadLine().Trim();

            //ask for message
            Console.ForegroundColor = optionsColor;
            Console.WriteLine("Write a message and press enter: ");
            Console.ForegroundColor = messageColor;
            string message = Console.ReadLine().Trim();

            //ask if it is part of a group
            Console.ForegroundColor = optionsColor;
            Console.WriteLine("Is this message part of a group? Enter (y/n) and press enter: ");
            Console.ForegroundColor = messageColor;
            bool groupMessage = (Console.ReadLine().Trim().ToLower().StartsWith("y")) ? true : false;

            //ask if it has any attachment
            Console.ForegroundColor = optionsColor;
            Console.WriteLine("Do you want to attach a file? Enter (y/n) and press enter: ");
            Console.ForegroundColor = messageColor;
            bool isMMS = (Console.ReadLine().Trim().ToLower().StartsWith("y")) ? true : false;

            //ask for file type
            if (isMMS)
            {
                Console.ForegroundColor = optionsColor;
                Console.WriteLine("Please enter the attachment filename and press enter: ");
                Console.ForegroundColor = messageColor;
                string filename = Console.ReadLine().Trim();

                bool choiceValid = false;

                do
                {
                    Console.ForegroundColor = headerColor;
                    Console.WriteLine("Please select the attachment type:");
                    Console.ForegroundColor = messageColor;
                    Console.WriteLine("\t1: Audio");
                    Console.WriteLine("\t2: Video");
                    Console.WriteLine("\t3: Picture");
                    Console.WriteLine("\t4: No attachment");
                    Console.ForegroundColor = headerColor;
                    Console.WriteLine("Select Option: ");
                    Console.ForegroundColor = messageColor;

                    string choice = Console.ReadLine().Trim();

                    switch (choice)
                    {
                        case "1":
                            attachmentType = MediaType.AUDIO;
                            choiceValid = true;
                            break;

                        case "2":
                            attachmentType = MediaType.VIDEO;
                            choiceValid = true;
                            break;

                        case "3":
                            attachmentType = MediaType.PICTURE;
                            choiceValid = true;
                            break;

                        case "4":
                            attachmentType = MediaType.NOATTACHMENT;
                            choiceValid = true;
                            break;
                        default:
                            Console.WriteLine("Invalid choice please try again.");
                            choiceValid = true;
                            break;
                    }
                }
                while (!choiceValid);
                return new MMS(sender, recipient, message, attachmentType, filename, groupMessage);
            } else
            {
                return new SMS(sender, recipient, message, groupMessage);
            }

        }

        // Dispaly the entire list of existing messages in the system
        private static void ShowAllMessages()
        {
            Console.ForegroundColor = headerColor;

            if (messages.Count == 0)
            {
                Console.ForegroundColor = errorColor;
                Console.WriteLine("\n******************");
                Console.WriteLine("* No messages in the sysytem! *");
                Console.WriteLine("******************");
            }
            else
            {
                Console.WriteLine("\n**********************");
                Console.WriteLine("* Show all messages: *");
                Console.WriteLine("**********************");
            }

            Console.ForegroundColor = messageColor;

            //loop through all messages (SMS and MMS) in the list
            for (int i = 0; i < messages.Count; i++)
            {
                Console.ForegroundColor = statusColor;
                Console.WriteLine("\t({0})", (i + 1));
                Status status = messages[i].status;
                Console.Write("\tStatus: ");

                if (status == Status.CREATED)
                {
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                }
                else if (status == Status.SENT)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                }
                else if (status == Status.RECEIVED)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                }
                Console.Write(status);

                Console.ForegroundColor = messageColor;
                string messageType = (messages[i].GetType() == typeof(MMS)) ? "MMS" : "SMS";
                Console.Write("\n\tMessage Type: {0}\n\tForm: {1}\n\tTo: {2}", messageType, messages[i].Sender, messages[i].Recipient + "\n");
            }
        }
    }

}
